package com.example.demo;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import service.CustomerService;

@SpringBootApplication
public class SpringIoCEx1Application {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		CustomerService service = context.getBean(CustomerService.class);
		
		System.out.println(service.supportType());
        
        context.close();
					
	}
}
