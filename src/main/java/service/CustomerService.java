package service;

import bean.CustomerPackage;

public class CustomerService {

	CustomerPackage custPackage;
	
	public void setCustomerPackage(CustomerPackage customerPackage) {
		this.custPackage = customerPackage;
			}

	public String supportType() {
		return custPackage.support();
	}
}
